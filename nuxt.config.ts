// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  // devtools: { enabled: true }
  css: ['vuetify/styles'],
  build: {
    transpile: ['vuetify']
  }
})
